// HWLinkedList.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <ctime> 
#include <fstream>
using namespace std;

struct Node {
	Node *next;
	int n;
};

struct Head {
	Node *first;
	int lenght;
};

class ExceptionBase {
	virtual void except();
};
class ExceptionLL : ExceptionBase {
	int numError;
public : ExceptionLL(int N) {
		numError = N;
	};
};

class Spisoc {
public: 
	Head head;
	Spisoc() {
		head.first = NULL;
		head.lenght = 0;
	}
	Spisoc(int m) {
		head.first = (Node *) new char[sizeof(Node)];
		head.first->next = NULL;
		head.first->n = m;
		head.lenght = 1;
	}
	~Spisoc() {
		Node *tmp = head.first;
		while (tmp->next != NULL) {
			Node *deleteTmp = tmp;
			tmp = tmp->next;
			delete deleteTmp;
		}
	}
	void Spisoc::Add(int m){
		Node *newNode = new Node;
		newNode->n = m;
		newNode->next = NULL;
		if (head.first != NULL) {	/*������ �������?*/
			Node *tmp = new Node;
			tmp = head.first;
			while (tmp->next != NULL) {/*����� "�����"*/
				tmp = tmp->next;
			}
			tmp->next = newNode;
		}
		else {
			head.first = newNode;
		}
		head.lenght++;
	}
	int& Spisoc::operator[] (int index) {
		if (index > head.lenght) throw new ExceptionLL(1);
		Node* tmpNode;
		tmpNode = head.first;
		for (int i = 1; i < index; i++)tmpNode = tmpNode->next;
		return tmpNode->n;
	}

	int Spisoc::Find(int m) {
		Node *tmp = head.first;
		int index = 0;
		while (tmp->next != NULL) {
			if (tmp->n == m)
				return index;
			tmp = tmp->next;
			index++;
		}
		if (tmp->n == m) {	/*���� ������� ��-� �� ��������� �����*/
			return index;
		}
		return -1;
	}
	int Spisoc::RequestValue(int index) {
		if (index > head.lenght-1) {	/*����� �� ��������*/
			return INT32_MAX;
		}
		Node *tmp = head.first;
		int cnt = 0;
		while (cnt < index) {
			tmp = tmp->next;
			cnt++;
		}
		return tmp->n;
	}
	void Spisoc::Remote(int index) {
		if (index > head.lenght - 1) { /*����� �� ��������*/
			return;
		}
		if (index == head.lenght - 1) {/*������� "�����"*/
			Node *deleteNode = new Node;
			Node *tmpSaveAdr = new Node;
			deleteNode = head.first;
			int cnt = 1;
			while (cnt < index) {	
				deleteNode = deleteNode->next;
				cnt++;
			}
			tmpSaveAdr = deleteNode;
			deleteNode = deleteNode->next;
			tmpSaveAdr->next = NULL;
			delete deleteNode;
			head.lenght--;
			return;
		}
		Node *deleteNode = new Node;
		Node *tmpSaveAdr = new Node;
		deleteNode = head.first;
		int cnt = 0;
		while (cnt < index - 1) {	/*������ �� ����� �� ���� ������ ������*/
			deleteNode = deleteNode->next;
			cnt++;
		}
		tmpSaveAdr = deleteNode;
		deleteNode = deleteNode->next;
		tmpSaveAdr->next = deleteNode->next;
		delete deleteNode;
		head.lenght--;
	}
};

int Input(void);
void PrintPrimeNumber(Spisoc *list);
void CalcPrimeNumbers(Spisoc *list);
void InsertPrimeNumbers(Spisoc *list, int len);
void Test(void);
void TestTime();
int main()
{
	Test();
	TestTime();
	Spisoc myList;
	int lenght = Input();
	InsertPrimeNumbers(&myList, lenght);
	unsigned int start_time = clock(); // ��������� �����
	CalcPrimeNumbers(&myList);
	unsigned int end_time = clock(); // �������� �����
	unsigned int search_time = end_time - start_time; // ������� �����
	cout << endl;
	cout << "time: " << search_time/1000.0 << endl;
	PrintPrimeNumber(&myList);
	system("pause");
	return 0;
}
#define LEN_TEST_TIME 10000
void TestTime () {
	Spisoc myList;
	int lenght = 1000;
	InsertPrimeNumbers(&myList, lenght);
	unsigned int arrayTime[LEN_TEST_TIME] = {};
	for (int i = 0; i < LEN_TEST_TIME; i++) {
		unsigned int start_time = clock(); // ��������� �����
		CalcPrimeNumbers(&myList);
		unsigned int end_time = clock(); // �������� �����
		unsigned int search_time = (end_time - start_time); // ������� �����
		arrayTime[i] = search_time;
	}
	ofstream fout("time.txt"); 
	for (int i = 0; i < LEN_TEST_TIME; i++) {
		fout << arrayTime[i] / 1000.0 << endl; // ������ ������ � ����
	}
	fout.close(); // ��������� ����

}

void Test(void) {
	int PrimeNum[] = {1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41,
						43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };
	Spisoc testList;
	InsertPrimeNumbers(&testList, 100);
	CalcPrimeNumbers(&testList);
	int cnt = 0;
	while (testList.RequestValue(cnt) != INT32_MAX){
		int impValue = testList.RequestValue(cnt);
		if (PrimeNum[cnt] != impValue) {
			cout << "Error1" << endl;
			break;
		}
		cnt++;
	}
}

int Input(void) {
	int tmp;
	cout << "Imput lenght: ";
	cin >> tmp;
	return tmp;
}

void InsertPrimeNumbers(Spisoc *list, int len) {
	for (int i = 1; i <= len; i++) {
		list->Add(i);
	}
}

void PrintPrimeNumber(Spisoc *list) {
	int out = 0;
	while (out < list->head.lenght) {
		cout << list->RequestValue(out) << " ";
		out++;
	}
	cout << endl;
}

void CalcPrimeNumbers(Spisoc *list) {
	int num = 1;
	while (true) {
		int value = list->RequestValue(num);

		int tmpNum = num + 1;
		int tmpValue = list->RequestValue(tmpNum);
		if (tmpValue == INT32_MAX) {
			break;
		}
		while (tmpValue != INT32_MAX) {
			if (!(tmpValue % value)) {
				int tmpIndex = list->Find(tmpValue);
				list->Remote(tmpIndex);
			}
			tmpNum++;
			tmpValue = list->RequestValue(tmpNum);
		}
		num++;
		if (num > list->head.lenght - 1) {
			break;
		}
	}


}
